#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include <SPI.h>


Adafruit_ST7735 display = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

unsigned long lastMenuAlternateTime = 0;  // Usado para controlar o período de alternância do menu

void setup_Display() {

  display.initR(INITR_BLACKTAB);

  printBlack();
  display.setTextSize(2);
  display.setRotation(1);  // Definindo o display para orientação horizontal
  
  initializeDisplayImage();
  
}

void refreshMenu() {
  
  bool isCurrentOn = getCurrentStateForBTCommand(btCommands[currentMenuIndex].command);

  printImageByName(btCommands[currentMenuIndex].image, isCurrentOn,btCommands[currentMenuIndex].title);

  printMenuOnSerial();
}

// Função para imprimir o log do menu no Serial
void printMenuOnSerial() {
  bool isCurrentOn = getCurrentStateForBTCommand(btCommands[currentMenuIndex].command);
  Serial.print("Menu Comando: ");
  Serial.print(btCommands[currentMenuIndex].command);
  Serial.print(" - ");
  Serial.print(btCommands[currentMenuIndex].description);
  Serial.print(" - ");
  Serial.println(isCurrentOn ? "ON" : "OFF");
}

void checkForInactivity() {
    // Se não houve ação por TIMEOUT_PERIOD
    if (millis() - lastActionTime > TIMEOUT_PERIOD) {
        
        // Se passou o período de alternância desde a última alternância
        if (millis() - lastMenuAlternateTime > ALTERNATE_PERIOD) {
            
            int originalMenuIndex = currentMenuIndex;
            int nextMenuIndex = currentMenuIndex;
            bool foundActiveOption = false;

            do {
                // Incrementa o nextMenuIndex para a próxima opção
                nextMenuIndex = (nextMenuIndex + 1) % (sizeof(btCommands) / sizeof(btCommands[0]));

                // Se encontramos uma opção ativa, atualize currentMenuIndex e marque foundActiveOption como verdadeiro
                if (getCurrentStateForBTCommand(btCommands[nextMenuIndex].command)) {
                    currentMenuIndex = nextMenuIndex;
                    foundActiveOption = true;
                    break;
                }
            } while (nextMenuIndex != originalMenuIndex); // Continua até chegar de volta ao menu original ou encontrar uma opção ativa

            // Se encontramos uma opção ativa, atualize o menu
            if (foundActiveOption) {
                refreshMenu();
            }
            
            // Atualiza o tempo da última alternância
            lastMenuAlternateTime = millis();
        }
    } else {
        // Se uma ação foi executada, reset o tempo da última alternância
        lastMenuAlternateTime = millis() - ALTERNATE_PERIOD;
    }
}

void printBlack() {
  display.fillScreen(ST7735_BLACK);
}