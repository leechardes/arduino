unsigned long esPreviousMillis = 0;
const long esInterval = 500;  // Tempo entre alternâncias
bool shouldRandomize = false;

struct ESCommand {
  const char* command;
  const char* description;
};

ESCommand esCommands[] = {
  { "ES01", "Ligar / Desligar Tudo" },
  { "ES02", "Ligar / Desligar Aleatoriamente" }
  // Você pode adicionar outros comandos ES aqui conforme necessário
};

const int ES_COMMAND_COUNT = sizeof(esCommands) / sizeof(esCommands[0]);

void processESCommands(const String& command) {
  String baseCommand;
  bool isOn = false;

  if (command.endsWith("ON")) {
    isOn = true;
    baseCommand = command.substring(0, command.length() - 2);
  } else if (command.endsWith("OFF")) {
    baseCommand = command.substring(0, command.length() - 3);
  } else {
    if (enableLogs) {
      Serial.print("Comando ES malformado: ");
      Serial.println(command);
      return;
    }
  }

  lastActionTime = millis();

  int esCommandPosition = findCommandPositionInESCommands(baseCommand.c_str());
  if (esCommandPosition != -1) {
    if (baseCommand == "ES01") {
      ProcessES01(isOn);
    } else if (baseCommand == "ES02") {
      processES02(isOn);
    } else {
      if (enableLogs) {
        Serial.print(esCommands[esCommandPosition].description);
        Serial.println(isOn ? " - ON" : " - OFF");
      }
      // Lógica de ligar/desligar para outros comandos ES, se houver
    }
  } else {
    if (enableLogs) {
      Serial.print("Comando ES desconhecido: ");
      Serial.println(command);
    }
  }
}

void ProcessES01(bool isOn) {
  // Array dos comandos BT a serem acionados quando ES01 é chamado
  const char* selectedCommands[] = { "BT01", "BT02", "BT08", "BT09", "BT10", "BT12" };
  const int selectedCommandsCount = sizeof(selectedCommands) / sizeof(selectedCommands[0]);

  for (int i = 0; i < selectedCommandsCount; i++) {
    for (int j = 0; j < BT_COMMAND_COUNT; j++) {
      if (String(selectedCommands[i]) == String(btCommands[j].command)) {
        if (enableLogs) {
          Serial.print(btCommands[j].description);
          Serial.println(isOn ? " - ON" : " - OFF");
        }

        // Lógica para ligar/desligar. Se você tiver uma função ou um método para fazer isso,
        // chame-a aqui. Por exemplo:
        // if (isOn) {
        //   ligar(btCommands[j].command);
        // } else {
        //   desligar(btCommands[j].command);
        // }
        break;
      }
    }
  }
}

const char* randomCommands[] = { "BT01", "BT02", "BT03", "BT04", "BT06", "BT07", "BT08", "BT09", "BT10", "BT11", "BT12" };
const int randomCommandsCount = sizeof(randomCommands) / sizeof(randomCommands[0]);

void processES02(bool isOn) {
  if (isOn && !shouldRandomize) {
    shouldRandomize = true;

    if (enableLogs) {
      Serial.println("Modo aleatório: LIGADO");
    }

    // Salvar o estado atual para posterior recuperação
    for (int i = 0; i < randomCommandsCount; i++) {
      for (int j = 0; j < BT_COMMAND_COUNT; j++) {
        if (String(randomCommands[i]) == String(btCommands[j].command)) {
          btCommands[j].previousState = btCommands[j].currentState;
          if (enableLogs) {
            Serial.print("Salvando estado anterior de ");
            Serial.print(btCommands[j].description);
            Serial.print(": ");
            Serial.println(btCommands[j].currentState ? "ON" : "OFF");
          }
        }
      }
    }
  } else if (!isOn && shouldRandomize) {
    shouldRandomize = false;

    if (enableLogs) {
      Serial.println("Modo aleatório: DESLIGADO");
    }

    // Recuperar o estado anterior
    for (int i = 0; i < randomCommandsCount; i++) {
      for (int j = 0; j < BT_COMMAND_COUNT; j++) {
        if (String(randomCommands[i]) == String(btCommands[j].command)) {
          btCommands[j].currentState = btCommands[j].previousState;

          if (enableLogs) {
            Serial.print("Restaurando estado anterior de ");
            Serial.print(btCommands[j].description);
            Serial.print(": ");
            Serial.println(btCommands[j].currentState ? "ON" : "OFF");
          }

          // Lógica para ligar/desligar o comando de volta ao seu estado anterior
          // Por exemplo: toggle(btCommands[j].command, btCommands[j].currentState);
        }
      }
    }
  }
}

// Função para encontrar a posição de um comando específico em esCommands
int findCommandPositionInESCommands(const char* command) {
  for (int i = 0; i < ES_COMMAND_COUNT; i++) {
    if (strcmp(esCommands[i].command, command) == 0) {
      return i;
    }
  }
  return -1;  // Retorna -1 se o comando não for encontrado
}

void handleRandomization() {
  if (shouldRandomize) {
    unsigned long currentMillis = millis();

    if (currentMillis - esPreviousMillis >= esInterval) {
      esPreviousMillis = currentMillis;
      // Salve o último momento em que você alternou

      int randomIndex = random(randomCommandsCount);
      for (int j = 0; j < BT_COMMAND_COUNT; j++) {
        if (String(randomCommands[randomIndex]) == String(btCommands[j].command)) {
          btCommands[j].currentState = !btCommands[j].currentState;
          // Lógica para ligar/desligar o comando
          // Por exemplo: toggle(btCommands[j].command, btCommands[j].currentState);

          // Se a variável enableLogs estiver ativa, imprima a ação
            char* commandString = buildCommandString();
            Serial.println(commandString);
            SerialBT.println(commandString);
          if (enableLogs) {
            Serial.print("Comando executado: ");
            Serial.print(btCommands[j].description);
            Serial.println(btCommands[j].currentState ? " - ON" : " - OFF");
          }
        }
      }
    }
  }
}