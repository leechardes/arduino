void processMNCommands(const String& command) {

  lastActionTime = millis();

  if (command == "MNNEXT") {
    navigateToNextMenu();
  } else if (command == "MNSELECT") {

    btCommands[currentMenuIndex].currentState = !btCommands[currentMenuIndex].currentState;

    if ( btCommands[currentMenuIndex].command == "BT17") {
      if (btCommands[currentMenuIndex].currentState) {
        bool updateState = true;
       
        if (getCurrentStateForBTCommand("BT01")) {
          updateState = false;
        } else if (getCurrentStateForBTCommand("BT02")) {
          updateState = false;
        }

        if (updateState) {
          updateBTCommandState("BT01", true);
        }

        updateBTCommandState("BT08", true);

      } else {
        updateBTCommandState("BT01", false);
        updateBTCommandState("BT02", false);
      }
    }

    refreshMenu();

    if (enableLogs) {
      bool isCurrentOn = btCommands[currentMenuIndex].currentState;
      Serial.print("Menu Comando: ");
      Serial.print(btCommands[currentMenuIndex].command);
      Serial.print(" - ");
      Serial.print(btCommands[currentMenuIndex].description);
      Serial.print(" - ");
      Serial.println(isCurrentOn ? "ON" : "OFF");
    }

  } else {
    if (enableLogs) {
      Serial.print("Comando MN desconhecido: ");
      Serial.println(command);
    }
  }
}