#include <BluetoothSerial.h>

BluetoothSerial SerialBT;
bool deviceConnected = false;

void processBTCommands();  // Protótipo da função
void processMNCommands();  // Protótipo da função
void processESCommands();  // Protótipo da função

char buffer[100];  // Ajuste o tamanho conforme necessário

void setup_Bluetooth() {

  SerialBT.begin("ControllerCar");  // Inicializa a comunicação Bluetooth
}

void readAndProcessBluetoothCommands() {
  static String currentCommand = "";

  while (SerialBT.available()) {
    char incomingChar = SerialBT.read();

    if (incomingChar == '#') {
      if (enableLogs) {
        Serial.print("Comando recebido: ");
        Serial.println(currentCommand);
      }
      processCommand(currentCommand);  // Processa o comando

      buildCommandString();

      currentCommand = "";
    } else {
      currentCommand += incomingChar;
    }
  }
}

void processCommand(const String& command) {
  if (command.startsWith("BT")) {
    processBTCommands(command);
  } else if (command.startsWith("MN")) {
    processMNCommands(command);  
  } else if (command.startsWith("ES")) {
    processESCommands(command);
  } else {
    if (enableLogs) {
      Serial.print("Comando desconhecido: ");
      Serial.println(command);
    }
  }
}

void printDeviceConnection() {
  if (SerialBT.hasClient()) {
    if (!deviceConnected) {
      deviceConnected = true;
      Serial.println("Dispositivo Conectado!"); 
    }
  } else {
    if (deviceConnected) {
      deviceConnected = false;
      Serial.println("Dispositivo Desconectado!"); 
    }
  }
}

char* buildCommandString() {
  
  Serial.println("Iniciando a construção da string de comando...");

  // Estimativa do tamanho máximo necessário para a string
  int numCommands = sizeof(btCommands) / sizeof(btCommands[0]);
  int maxSize = numCommands * (strlen(btCommands[0].command) + 2); // +2 para o caractere de estado e o separador
  char* strCommand = (char*) malloc(maxSize + 1); // +1 para o caractere nulo '\0'
  if (!strCommand) {
    Serial.println("Erro ao alocar memória!");
    return NULL;
  }
  strCommand[0] = '\0'; // Inicializa a string vazia

  for (int i = 0; i < numCommands; i++) {
    char stateChar = btCommands[i].currentState ? 'H' : 'L';
    char temp[50]; // Buffer temporário para formatação
    if (i == numCommands - 1) { // Se for o último comando
      sprintf(temp, "%s%c", btCommands[i].command, stateChar);
    } else {
      sprintf(temp, "%s%c;", btCommands[i].command, stateChar);
    }
    strcat(strCommand, temp);
  }

  strcat(strCommand, "#");
  
  Serial.println(strCommand);

  SerialBT.println(strCommand);

  return strCommand;
}
