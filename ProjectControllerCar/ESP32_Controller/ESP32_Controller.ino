// Os pinos com pull up interno são os seguintes:
// GPIO_14 - Avançar
// GPIO_16 - Selecionar
// GPIO_17 - Pisca Direito
// GPIO_18 - Display
// GPIO_19 - Pisca Esquerdo
// GPIO_21 - Luz Alta
// GPIO_22 - Luz Baixa
// GPIO_23 - Display

// Os pinos sem pull up interno são os seguintes:
// GPIO_13 - Display
// GPIO_25 - Display
// GPIO_26 - Display
// GPIO_27 - Buzina
// GPIO_32 - Freio
// GPIO_33 - Luz de Ré

// Portas Display
#define TFT_CS 13 
#define TFT_DC 25 
#define TFT_RST 26 

enum ButtonType {
  ToggleState,
  FirstWhere,
};

enum ButtonState {
  RELEASED,
  PRESSED,
  LONG_PRESSED
};

#define TIMEOUT_PERIOD 10000  // 10 segundos de inatividade
#define ALTERNATE_PERIOD 5000  // 5 segundos de alternância

bool isBT03Blinking = false;
bool isBT04Blinking = false;
bool isAlertBlinking = false;

bool alertImagePrintedThisCycle = false;

unsigned long lastBT03BlinkTime = 0;
unsigned long lastBT04BlinkTime = 0;
unsigned long lastAlertBlinkTime = 0;

const unsigned long blinkInterval = 500;  // Intervalo de 500 ms

unsigned long lastActionTime = 0; // Tempo da última ação realizada

bool enableLogs = true;  // Mude para 'false' para desabilitar logs

unsigned long previousMillis = 0;
const long interval = 100000;  // 10000 ms, ou 10 segundos

int currentMenuIndex = 0;

const unsigned long debounceDelay = 50;  // Tempo de debounce em milissegundos

struct BTCommand {
  const char* command;
  const char* description;
  bool currentState;   // Estado atual do comando (ligado/desligado)
  bool previousState;  // Estado anterior para reverter após o comando ESP02
  int type;            // Tipo do botão
  bool menuEnable;     // Habilita no display, lembrando que trabalha em conjunto com image e title
  const char* image;   // Imagem a ser exibida 
  const char* title;   // Titulo a ser exibido
};

BTCommand btCommands[] = {
  { "BT17", "Farol", false, false, ToggleState, true,  "LEDFAROL", "LED FAROL" },
  { "BT18", "Alerta",false, false, ToggleState, true, "ALERTA", "ALERTA" },
  { "BT01", "Luz baixa", false, false, ToggleState, false, "", "" },
  { "BT02", "Luz alta", false, false, ToggleState, false, "", "" },
  { "BT03", "Pisca direito", false, false, ToggleState, false, "", "" },
  { "BT04", "Pisca esquerdo", false, false, ToggleState, false, "", "" },
  { "BT05", "Buzina", false, false, FirstWhere, false, "", "" },
  { "BT06", "Luz freio", false, false, ToggleState, false, "", "" },
  { "BT07", "Luz Ré", false, false, ToggleState, false, "", "" },
  { "BT08", "Luz de estacionamento", false, false, ToggleState, true, "LEDESTACIONAMENTO", "LED ESTACIONAMENTO" },
  { "BT09", "Led parachoque dianteiro", false, false, ToggleState, true, "LEDDIANTEIRO", "LED DIANTEIRO" },
  { "BT10", "Led parachoque traseiro", false, false, ToggleState, true, "LEDTRASEIRO", "LED TRASEIRO" },
  { "BT11", "Led capo", false, false, ToggleState, true, "LEDMOTOR", "LED MOTOR" },
  { "BT12", "Barra de led", false, false, ToggleState, true, "LEDBARRA", "LED BARRA" },
  { "BT13", "Guincho IN", false, false, FirstWhere, true, "GUINCHO", "GUINCHO IN"  },
  { "BT14", "Guincho OUT", false, false, FirstWhere, true, "GUINCHO", "GUINCHO OUT"  },
  { "BT15", "Bloqueio Dianteiro", false, false, ToggleState, true, "BLOQUEIO", "BLOQUEIO DIANTEIRO" },
  { "BT16", "Bloqueio Traseiro", false, false, ToggleState, true, "BLOQUEIO", "BLOQUEIO TRASEIRO" }
};

// Tamanho da lista de comandos MN
const int BT_COMMAND_COUNT = sizeof(btCommands) / sizeof(btCommands[0]);

// Protótipo da função
void setup_printFlashMemoryStats();      
void setup_Display();                    
void setup_Bluetooth();                  
void setup_SerialCommunication();        
void printFlashMemoryStats();            
void printMemoryStats();                 
void printDeviceConnection();            
void readAndProcessBluetoothCommands();  
void handleRandomization();              
void updateBlinkingStates();             

void setup() {

  Serial.begin(115200);

  setup_printFlashMemoryStats();
  setup_Display();
  setup_Bluetooth();
  setup_SerialCommunication();
  
  // Mensagem indicando que o setup foi concluído.
  if (enableLogs) {
    Serial.println("Setup concluído com sucesso!");
  }
  refreshMenu();
}

void loop() {

  readAndProcessBluetoothCommands();
  handleRandomization();
  updateBlinkingStates();
  //checkForInactivity();
  updateButtonInputs();
  updateTurnSignals();
  
  unsigned long currentMillis = millis();

  // Verifique se 10 segundos se passaram; note que isto também funcionará quando millis() desbordar.
  if (currentMillis - previousMillis >= interval) {

    previousMillis = currentMillis;

    if (enableLogs) {
      Serial.println("==== STATUS DA MEMÓRIA ====");
      printFlashMemoryStats();
      printMemoryStats();
      printDeviceConnection();
      Serial.println("============================");
    }
  }
}