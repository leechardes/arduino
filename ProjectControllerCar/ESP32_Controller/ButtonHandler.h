#ifndef BUTTON_HANDLER_H
#define BUTTON_HANDLER_H

#include <Arduino.h>

class ButtonHandler {
public:
    ButtonHandler(uint8_t pin);
    void update();
    bool isPressed();  // Retorna true se o botão foi pressionado
    bool isReleased(); // Retorna true se o botão foi solto

private:
    uint8_t pin;
    bool currentState;
    bool previousState;
    unsigned long lastDebounceTime;
    bool buttonPressed;
    const unsigned long debounceDelay = 50;  // Ajuste este valor conforme necessário
};

#endif
