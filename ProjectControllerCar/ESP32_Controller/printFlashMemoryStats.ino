#include <SPIFFS.h>

void setup_printFlashMemoryStats() {

    if (!SPIFFS.begin(true)) {  // Inicializa o SPIFFS. O parâmetro 'true' formatará o sistema de arquivos se necessário.
        Serial.println("Erro ao montar SPIFFS");
        return;
    }

}

void printFlashMemoryStats() {

    Serial.print("Mémoria Flash Total: ");
    Serial.print(SPIFFS.totalBytes() / 1024);
    Serial.println(" KB");

    Serial.print("Usados: ");
    Serial.print(SPIFFS.usedBytes());
    Serial.println(" KB");

    Serial.print("Livres: ");
    Serial.print((SPIFFS.totalBytes() - SPIFFS.usedBytes()) / 1024);
    Serial.println(" KB");
}