#include "ButtonHandler.h"

ButtonHandler::ButtonHandler(uint8_t pin) : pin(pin), previousState(HIGH), lastDebounceTime(0), buttonPressed(false) {
    pinMode(pin, INPUT_PULLUP);
}

void ButtonHandler::update() {
    
    bool reading = digitalRead(pin);
    
    if ( reading != previousState ) {
        lastDebounceTime = millis();
        previousState = reading;
        Serial.println("Transição de HIGH para LOW detectada.");
    }
}

bool ButtonHandler::isPressed() {
  
    bool currentState = digitalRead(pin);

    if (currentState == LOW && !buttonPressed && (millis() - lastDebounceTime) > debounceDelay) {
        buttonPressed = true;
        Serial.println("Botão pressionado detectado.");
        return true;
    }
    return false;

}

bool ButtonHandler::isReleased() {

    bool currentState = digitalRead(pin);

    if (currentState == HIGH && buttonPressed) {
        buttonPressed = false;
        Serial.println("Botão solto detectado.");
        return true;
    }

    return false;
}
