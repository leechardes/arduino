void processBTCommands(const String& command) {

  String baseCommand;
  bool validCommand = false;
  bool isOn = false;

  // Verificar se o comando tem ON ou OFF no final
  if (command.endsWith("ON")) {
    isOn = true;
    baseCommand = command.substring(0, command.length() - 2);
  } else if (command.endsWith("OFF")) {
    baseCommand = command.substring(0, command.length() - 3);
  } else {
    if (enableLogs) {
      Serial.print("Comando BT malformado: ");
      Serial.println(command);
      return;
    }
  }

  lastActionTime = millis();

  int btCommandPosition = findCommandPositionInBTCommands(baseCommand.c_str());

  if (btCommandPosition != -1) {
    validCommand = true;

    // Atualizando o estado atual do comando encontrado
    btCommands[btCommandPosition].currentState = isOn;

    // Tratamento específico para BT01, BT02, BT03, BT04, BT17 e BT18
    if (baseCommand == "BT01") {

      updateBTCommandState("BT02", false);
      updateBTCommandState("BT17", isOn);

      currentMenuIndex = findCommandPositionInBTCommands("BT17");
      refreshMenu();

    } else if (baseCommand == "BT02") {

      updateBTCommandState("BT01", false);
      updateBTCommandState("BT17", isOn);

      currentMenuIndex = findCommandPositionInBTCommands("BT17");
      refreshMenu();

    } else if (baseCommand == "BT03") {

      isBT03Blinking = isOn;

      if (isBT03Blinking == true && isBT04Blinking == true) {
        isBT04Blinking = false;
      }
      if (!isOn) {
        refreshMenu();
      }

    } else if (baseCommand == "BT04") {

      isBT04Blinking = isOn;

      if (isBT04Blinking == true && isBT03Blinking == true) {
        isBT03Blinking = false;
      }

      if (!isOn) {
        refreshMenu();
      }
    } else if (baseCommand == "BT17") {

      if (isOn) {
        bool updateState = true;
        //TODO Quando clicar e o farol ligar, a luz alta ou baixa deve depender do estado da seta.
        if (getCurrentStateForBTCommand("BT01")) {
          updateState = false;
        } else if (getCurrentStateForBTCommand("BT02")) {
          updateState = false;
        }

        if (updateState) {
          updateBTCommandState("BT01", true);
        }

        updateBTCommandState("BT08", true);

      } else {
        updateBTCommandState("BT01", false);
        updateBTCommandState("BT02", false);
      }

    } else if (baseCommand == "BT18") {

      isAlertBlinking = isOn;

      if (isAlertBlinking == true && isBT03Blinking == true) {
        isBT03Blinking = false;
      }

      if (isAlertBlinking == true && isBT04Blinking == true) {
        isBT04Blinking = false;
      }

      if (!isOn) {
        refreshMenu();
      }
    }

    // Atualize o currentMenuIndex baseado no comando encontrado
    if (btCommands[btCommandPosition].menuEnable) {
      currentMenuIndex = btCommandPosition;
      refreshMenu();
    }

    // Processar o comando específico
    if (enableLogs) {
      Serial.print(btCommands[btCommandPosition].description);
      Serial.println(isOn ? " - ON" : " - OFF");
    }
  }

  if (!validCommand && enableLogs) {
    Serial.print("Comando BT desconhecido: ");
    Serial.println(command);
  }
}