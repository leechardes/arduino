void navigateToNextMenu() {
    do {
        currentMenuIndex = (currentMenuIndex + 1) % BT_COMMAND_COUNT;
    } while (!btCommands[currentMenuIndex].menuEnable);
    
    refreshMenu();
}

bool getCurrentStateForBTCommand(const char* commandToFind) {
  int position = findCommandPositionInBTCommands(commandToFind);
  if (position != -1) {
    return btCommands[position].currentState;
  }
  return false;  // Retorna false se o comando não for encontrado
}

void toggleCommandState(const char* commandToToggle) {
  
  int position = findCommandPositionInBTCommands(commandToToggle);

  lastActionTime = millis();

  if (position != -1) {
    btCommands[position].currentState = !btCommands[position].currentState;

    if (btCommands[position].currentState) {
      if (isAlertBlinking) {
        if (!alertImagePrintedThisCycle) {
          printImageByName("ALERTA", btCommands[position].currentState, "ALERTA");
          alertImagePrintedThisCycle = true;
        }
      } else {
        if (strcmp(commandToToggle, "BT03") == 0) {
          printImageByName("SETADIREITA", btCommands[position].currentState, "SETA DIREITA");
        } else if (strcmp(commandToToggle, "BT04") == 0) {
          printImageByName("SETAESQUERDA", btCommands[position].currentState, "SETA ESQUERDA");
        }
      }
    } else {
      printBlack();
    }
  }
}

int findCommandPositionInBTCommands(const char* commandToFind) {
  for (int i = 0; i < BT_COMMAND_COUNT; i++) {
    if (strcmp(btCommands[i].command, commandToFind) == 0) {
      return i;
    }
  }
  return -1;  // Retorna -1 se o comando não for encontrado
}

void updateBTCommandState(const char* command, bool newState) {

  int commandPosition = findCommandPositionInBTCommands(command);

  if (commandPosition != -1) {
    btCommands[commandPosition].currentState = newState;
    buildCommandString();

    if (btCommands[commandPosition].menuEnable){
      currentMenuIndex = commandPosition;
      refreshMenu();
    }
  } else {
    if (enableLogs) {
      Serial.print("Comando não encontrado para atualização: ");
      Serial.println(command);
    }
  }
}

void updateBlinkingStates() {

  alertImagePrintedThisCycle = false;

  if (isAlertBlinking && (millis() - lastAlertBlinkTime) > blinkInterval) {
    toggleCommandState("BT03");
    toggleCommandState("BT04");
    lastAlertBlinkTime = millis();
  } else {
    if (isBT03Blinking && (millis() - lastBT03BlinkTime) > blinkInterval) {
      toggleCommandState("BT03");
      lastBT03BlinkTime = millis();
    }

    if (isBT04Blinking && (millis() - lastBT04BlinkTime) > blinkInterval) {
      toggleCommandState("BT04");
      lastBT04BlinkTime = millis();
    }
  }
}
