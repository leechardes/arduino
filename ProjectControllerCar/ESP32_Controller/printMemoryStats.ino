#include <esp_heap_caps.h>

void printMemoryStats() {
  
    Serial.print("Heap total: ");
    Serial.print(ESP.getHeapSize() / 1024.0); // Total heap size in KB
    Serial.println(" KB");

    Serial.print("Heap disponível: ");
    Serial.print(ESP.getFreeHeap() / 1024.0); // Available heap size in KB
    Serial.println(" KB");
}