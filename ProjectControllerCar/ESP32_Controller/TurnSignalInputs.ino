#include "ButtonHandler.h"

struct TurnSignals {
  const char* command;
  const char* description;
  ButtonHandler handler;
  bool lastState;
};

TurnSignals turnSignals[] = {
  { "BT01", "Luz Baixa", ButtonHandler(17) },
  { "BT02", "Luz Alta", ButtonHandler(19) },
  { "BT03", "Pisca Direito", ButtonHandler(21) },
  { "BT04", "Pisca Esquerdo", ButtonHandler(22) },
  { "BT05", "Buzina", ButtonHandler(27) },
  { "BT06", "Luz de Freio", ButtonHandler(32) },
  { "BT07", "Luz de Ré", ButtonHandler(33) },
};

// Tamanho da lista de comandos MN
const int turnSignalCount = sizeof(turnSignals) / sizeof(turnSignals[0]);

unsigned long turnSignalsLastDebounceTime = 0;

void updateTurnSignals() {

  for (int i = 0; i < turnSignalCount; i++) {

    turnSignals[i].handler.update();

    if (turnSignals[i].handler.isPressed()) {
      int position = findCommandPositionInBTCommands(turnSignals[i].command);
      updateBTCommandState(turnSignals[i].command, true);
    }

    if (turnSignals[i].handler.isReleased()) {
      int position = findCommandPositionInBTCommands(turnSignals[i].command);
      updateBTCommandState(turnSignals[i].command, false);
    }
  }
}