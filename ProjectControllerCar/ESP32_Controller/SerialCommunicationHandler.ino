#include <HardwareSerial.h>

// Configuração da Serial1 para comunicação com o Arduino Mega
#define RX_PIN 16 // Pino RX, por exemplo, 16.
#define TX_PIN 17 // Pino TX, por exemplo, 17.

HardwareSerial MegaSerial(1);

void setup_SerialCommunication() {

  MegaSerial.begin(115200, SERIAL_8N1, RX_PIN, TX_PIN);
}

void sendDataToMega(String command){
  
  if (enableLogs) {
    Serial.print("sendDataToMega comando: ");
    Serial.println(command);
  }
  
  MegaSerial.println(command);  // Envia o comando para o Mega via Serial1
};

void receiveDataFromMega(){

};