#include "ButtonHandler.h"

struct ButtonInputs {
  const char* description;
  ButtonHandler handler;
  const char* command;  // Adicionado para rastrear se a ação do botão já foi executada
};

ButtonInputs buttonInputs[] = {
  { "Avancar", ButtonHandler(14), "NEXT" },
  { "Selecionar", ButtonHandler(16), "SELECT" }
};

unsigned long buttonInputsLastDebounceTime = 0;
const int buttonInputCount = sizeof(buttonInputs) / sizeof(buttonInputs[0]);

void updateButtonInputs() {

  for (int i = 0; i < buttonInputCount; i++) {

    buttonInputs[i].handler.update();

    if (buttonInputs[i].handler.isPressed()) {

      if (strcmp(buttonInputs[i].command, "NEXT") == 0) {
        navigateToNextMenu();
      } else if (strcmp(buttonInputs[i].command, "SELECT") == 0) {       
        if (btCommands[currentMenuIndex].type == FirstWhere) {
          updateBTCommandState(btCommands[currentMenuIndex].command, true);
        } else {
          updateBTCommandState(btCommands[currentMenuIndex].command, !btCommands[currentMenuIndex].currentState);
        }
      }
    }

    if (buttonInputs[i].handler.isReleased()) {

      if (btCommands[currentMenuIndex].type == FirstWhere) {
        updateBTCommandState(btCommands[currentMenuIndex].command, false);
      }
    }
  }
}