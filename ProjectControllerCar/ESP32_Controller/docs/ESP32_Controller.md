# ESP32_Controller.ino

## Descrição Geral

Este arquivo é o principal do projeto e contém a lógica de inicialização e loop principal para o controle do ESP32.

## Variáveis Globais

- **enableLogs**: Controla se os logs devem ser impressos ou não.
- **previousMillis**: Armazena o último valor registrado de millis().
- **interval**: Intervalo de tempo para verificar o status da memória.
- **currentMenuIndex**: Índice do menu atualmente selecionado.
- **menuOptions**: Lista de opções do menu.
- **MENU_ITEMS**: Tamanho da lista de opções do menu.
- **btCommands**: Lista de comandos Bluetooth.
- **BT_COMMAND_COUNT**: Tamanho da lista de comandos Bluetooth.

## Estruturas

### 1. `MenuItem`

Representa um item do menu.

- **command**: Comando associado ao item.
- **description**: Descrição do item.
- **image**: Imagem associada ao item.
- **title**: Título do item.

### 2. `BTCommand`

Representa um comando Bluetooth.

- **command**: Comando.
- **description**: Descrição do comando.
- **currentState**: Estado atual do comando.
- **previousState**: Estado anterior do comando.

## Funções

### 1. `setup()`

Função de inicialização. Configura a comunicação serial, inicializa as funções de setup específicas e imprime uma mensagem indicando que o setup foi concluído.

### 2. `loop()`

Loop principal. Processa comandos Bluetooth, atualiza a seleção do menu, lida com a randomização e atualiza os estados de piscar. Além disso, verifica periodicamente o status da memória.

## Resumo

O arquivo `ESP32_Controller.ino` é o núcleo do projeto e contém a lógica principal para o funcionamento do ESP32. Ele gerencia a inicialização, processamento de comandos Bluetooth, atualização de menu, randomização e monitoramento do status da memória.