# ButtonInputs.ino

## Descrição Geral

Este arquivo contém funções e estruturas relacionadas à manipulação e leitura de botões no ESP32.

## Estruturas

### 1. `Button`

Representa um botão físico.

- **description**: Descrição ou nome do botão.
- **pin**: Pino ao qual o botão está conectado.
- **lastState**: Estado anterior do botão (HIGH ou LOW).

```c
struct Button {
  const char* description;
  const uint8_t pin;
  bool lastState;
};
```

### 2. `ButtonIndex`

Enumeração para identificar os botões.

- **AVANCAR**: Botão para avançar.
- **VOLTAR**: Botão para voltar.
- **SELECIONAR**: Botão para selecionar.
- **BUTTON_COUNT**: Contador de botões.

```c
enum ButtonIndex {
  AVANCAR,
  VOLTAR,
  SELECIONAR,
  BUTTON_COUNT  // Sempre no final para dar o total de botões
};
```

## Variáveis Globais

- **buttons**: Array de botões.
- **lastDebounceTime**: Último tempo de debounce registrado.
- **debounceDelay**: Tempo de debounce em milissegundos.

## Funções

### 1. `setup_ButtonInputs()`

Configura os pinos dos botões como entrada com pull-up.

```c
void setup_ButtonInputs() {
  for (int i = 0; i < BUTTON_COUNT; i++) {
    pinMode(buttons[i].pin, INPUT_PULLUP);
  }
}
```

### 2. `updateMenuSelection()`

Atualiza a seleção do menu com base na entrada do botão. Implementa a lógica de debounce para evitar leituras falsas.

```c
void updateMenuSelection() {
  // ... (lógica de leitura e atualização do botão)
}
```

### 3. `toggleCurrentStateForCommand(const char* command)`

Alterna o estado atual para um comando específico.

```c
void toggleCurrentStateForCommand(const char* command) {
  // ... (lógica de alternância do estado do comando)
}
```

## Resumo

O arquivo `ButtonInputs.ino` é focado em fornecer funções e estruturas para manipular e ler botões no ESP32. Ele contém lógica para atualizar a seleção do menu com base na entrada do botão e para alternar o estado de um comando específico.