# DisplayHandler.ino

## Descrição Geral

Este arquivo contém funções e variáveis relacionadas à manipulação e exibição de informações no display do ESP32.

## Inclusões

```c
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include <SPI.h>
```

O arquivo inclui as bibliotecas `Adafruit_GFX.h`, `Adafruit_ST7735.h` e `SPI.h`, que são usadas para manipular e exibir gráficos no display TFT ST7735.

## Variáveis Globais

- **TFT_CS, TFT_DC, TFT_RST**: Definições de pinos para o display.
- **display**: Objeto para manipulação do display.

## Funções

### 1. `setup_Display()`

Inicializa o display, define sua orientação, tamanho do texto e preenche a tela com a cor preta.

### 2. `refreshMenu()`

Atualiza a exibição do menu no display. Esta função verifica o estado atual do comando selecionado e imprime a imagem e o título correspondentes no display.

### 3. `printMenuOnSerial()`

Imprime o log do menu no Serial. Esta função é útil para depuração e monitoramento.

## Resumo

O arquivo `DisplayHandler.ino` é focado em fornecer funções para manipular e exibir informações no display do ESP32. Ele contém lógica para inicializar o display, atualizar a exibição do menu e imprimir logs no Serial.