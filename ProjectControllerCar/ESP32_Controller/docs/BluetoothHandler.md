# BluetoothHandler.ino

## Descrição Geral

Este arquivo contém funções e variáveis relacionadas à manipulação e processamento de comandos via Bluetooth no ESP32.

## Inclusões

```c
#include <BluetoothSerial.h>
```

O arquivo inclui a biblioteca `BluetoothSerial.h`, que é usada para manipular a comunicação Bluetooth no ESP32.

## Variáveis Globais

- **SerialBT**: Objeto para comunicação Bluetooth.
- **deviceConnected**: Indica se um dispositivo está conectado via Bluetooth.

## Funções

### 1. `setup_Bluetooth()`

Inicializa a comunicação Bluetooth com o nome "ControllerCar".

### 2. `readAndProcessBluetoothCommands()`

Lê e processa comandos recebidos via Bluetooth. Os comandos são delimitados pelo caractere '#'.

### 3. `processCommand(const String& command)`

Processa um comando específico. Esta função verifica o prefixo do comando (BT, MN ou ES) e chama a função de processamento correspondente.

### 4. `printDeviceConnection()`

Verifica e imprime o status da conexão Bluetooth (conectado ou desconectado).

## Resumo

O arquivo `BluetoothHandler.ino` é focado em fornecer funções para manipular e processar comandos via Bluetooth no ESP32. Ele contém lógica para inicializar a comunicação Bluetooth, ler e processar comandos recebidos e verificar o status da conexão.