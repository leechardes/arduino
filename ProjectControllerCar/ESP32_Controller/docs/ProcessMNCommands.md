# ProcessMNCommands.ino

## Descrição Geral

Este arquivo contém funções e estruturas relacionadas ao processamento de comandos MN no ESP32.

## Estruturas

### 1. `MNCommand`

Representa um comando MN.

- **command**: Comando.
- **description**: Descrição do comando.

```c
struct MNCommand {
  const char* command;
  const char* description;
};
```

## Variáveis Globais

- **mnCommands**: Lista de comandos MN.
- **MN_COMMAND_COUNT**: Tamanho da lista de comandos MN.

## Funções

### 1. `processMNCommands(const String& command)`

Processa um comando MN específico. Esta função lida com os comandos MNBACK, MNNEXT e MNSELECT, atualizando o índice do menu e o estado dos comandos conforme necessário.

### 2. `findPositionInMNCommands(const char* command)`

Encontra a posição de um comando específico na lista `mnCommands`.

- **Parâmetros**: Comando a ser procurado.
- **Retorno**: Retorna a posição do comando na lista ou -1 se o comando não for encontrado.

## Resumo

O arquivo `ProcessMNCommands.ino` é focado em fornecer funções e estruturas para processar comandos MN no ESP32. Ele contém lógica para lidar com a seleção e atualização do menu com base nos comandos MN recebidos.