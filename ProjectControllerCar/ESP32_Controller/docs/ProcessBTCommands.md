# Documentação do arquivo `ProcessBTCommands.ino`

Este arquivo é responsável por processar comandos Bluetooth e atualizar o estado de certos comandos, bem como lidar com a lógica de piscar para diferentes comandos.

## Variáveis Globais

- `isBT03Blinking`: Indica se o BT03 está piscando.
- `isBT04Blinking`: Indica se o BT04 está piscando.
- `isAlertBlinking`: Indica se um alerta está piscando.
- `alertImagePrintedThisCycle`: Indica se a imagem de alerta foi impressa neste ciclo.
- `lastBT03BlinkTime`: Armazena a última vez que o BT03 piscou.
- `lastBT04BlinkTime`: Armazena a última vez que o BT04 piscou.
- `lastAlertBlinkTime`: Armazena a última vez que um alerta piscou.
- `blinkInterval`: Intervalo de tempo para piscar (500 ms).

## Funções

### `processBTCommands(const String& command)`

Esta função processa os comandos Bluetooth recebidos.

- Verifica se o comando termina com "ON" ou "OFF".
- Atualiza o estado atual do comando encontrado.
- Realiza tratamento específico para os comandos BT03, BT04 e BT18.
- Atualiza o índice do menu atual com base no comando encontrado.
- Processa o comando específico.

### `updateBlinkingStates()`

Atualiza os estados de piscar.

- Verifica se o alerta está piscando e atualiza os estados de BT03 e BT04.
- Atualiza os estados de BT03 e BT04 individualmente se eles estiverem piscando.

### `toggleCommandState(const char* commandToToggle)`

Alterna o estado do comando.

- Encontra a posição do comando na lista de comandos.
- Alterna o estado atual do comando.
- Imprime a imagem correspondente com base no estado e no comando.

### `findCommandPositionInBTCommands(const char* commandToFind)`

Encontra a posição do comando na lista de comandos.

- Retorna a posição do comando se encontrado.
- Retorna -1 se o comando não for encontrado.

### `getCurrentStateForBTCommand(const char* commandToFind)`

Obtém o estado atual para o comando Bluetooth.

- Retorna o estado atual do comando se encontrado.
- Retorna false se o comando não for encontrado.

### `findPositionInMenuOptions(const char* commandToFind)`

Encontra a posição do comando nas opções do menu.

- Retorna a posição da opção do menu se encontrada.
- Retorna -1 se a opção não for encontrada.