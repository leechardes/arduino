## Mapeamentos de Imagens

Aqui estão os mapeamentos das imagens:

```cpp
ImageMapping imageMappings[] = {
    {"TRACAO4X2", TRACAO4X2},
    //... outros mapeamentos
};
```

## Função: `printImageByName`

Mostra uma imagem específica no display baseada em seu nome.

- **Parâmetros**:
  - `imageName`: Nome da imagem a ser mostrada.
  - `isOn`: Booleano que indica se a imagem deve ser mostrada com cor verde (verdadeiro) ou branca (falso).
  - `title`: Título a ser mostrado abaixo da imagem.

```cpp
void printImageByName(const char* imageName, bool isOn, const char* title);
```

## Função: `initializeDisplayImage`

Inicializa o display mostrando uma imagem inicial por 3 segundos e depois limpando a tela.

```cpp
void initializeDisplayImage();
```

## Função: `updateLine`

Atualiza uma linha específica do display com um texto fornecido.

- **Parâmetros**:
  - `line`: A linha do display a ser atualizada.
  - `text`: O texto a ser mostrado na linha.
  - `textColor`: Cor do texto.
  - `bgColor`: Cor de fundo.
  - `textSize`: Tamanho do texto.

```cpp
void updateLine(uint8_t line, const char* text, uint16_t textColor, uint16_t bgColor, uint8_t textSize);
```

---

Esta é uma descrição básica das funções e estruturas utilizadas no projeto. A documentação pode ser expandida conforme a necessidade, incluindo mais detalhes, exemplos ou especificidades técnicas.
```

---

Agora, o texto está corrigido e pronto para ser utilizado. Se precisar de mais ajustes ou outras informações, por favor, me avise!