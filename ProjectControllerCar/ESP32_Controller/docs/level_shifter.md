## Utilizando conversor l�gico (level shifter)

O conversor l�gico � usado para converter os n�veis de tens�o entre os dispositivos que operam em diferentes voltagens. Isso garante que voc� n�o danificar� seus dispositivos ao conect�-los juntos.

Vamos ao passo a passo:

## Montagem:

1. **Alimenta��o do Conversor L�gico**: Conecte os pinos `LV` (Low Voltage) e `GND` do conversor l�gico aos pinos `3.3V` e `GND` do ESP32, respectivamente. Conecte os pinos `HV` (High Voltage) e o `GND` do outro lado do conversor l�gico aos pinos `5V` e `GND` do Arduino Mega, respectivamente.

2. **ESP32 TX para Arduino Mega RX**: Conecte o pino `TX` do ESP32 (por exemplo, GPIO17 se voc� estiver usando `Serial1`) ao pino `LV1` (ou outro pino LV correspondente) do conversor l�gico. Conecte o pino `RX1` (pin 19) do Arduino Mega ao pino `HV1` (ou outro pino HV correspondente) do conversor l�gico.

3. **Arduino Mega TX para ESP32 RX**: Conecte o pino `TX1` (pin 18) do Arduino Mega ao pino `HV2` (ou outro pino HV correspondente) do conversor l�gico. Conecte o pino `RX` do ESP32 (por exemplo, GPIO16 se voc� estiver usando `Serial1`) ao pino `LV2` (ou outro pino LV correspondente) do conversor l�gico.

## C�digo exemplo para o ESP32:

```cpp
#define RXD2 16
#define TXD2 17

void setup() {
  Serial.begin(115200);          // Comunica��o para debug
  Serial1.begin(115200, SERIAL_8N1, RXD2, TXD2);
}

void loop() {
  Serial1.println("Ol� Arduino Mega!");
  delay(1000);
}
```

## C�digo exemplo para o Arduino Mega:

```cpp
void setup() {
  Serial.begin(115200);  // Come�a comunica��o serial para debug
  Serial1.begin(115200); // Come�a comunica��o serial no RX1/TX1
}

void loop() {
  if (Serial1.available()) {    // Verifica se h� dados dispon�veis na Serial1
    String message = Serial1.readString();  // L� a string da Serial1
    Serial.println(message);  // Imprime a string na Serial
  }
}
```

Lembre-se de que usar um conversor l�gico � uma abordagem mais confi�vel e segura do que usar divisores de tens�o, especialmente para comunica��es mais r�pidas ou para cen�rios onde a integridade do sinal � crucial.