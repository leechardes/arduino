# printFlashMemoryStats.ino

## Descrição Geral

Este arquivo contém funções relacionadas à obtenção e exibição de estatísticas da memória flash do ESP32.

## Inclusões

```c
#include <SPIFFS.h>
```

O arquivo inclui a biblioteca `SPIFFS.h`, que é usada para manipular o sistema de arquivos SPIFFS no ESP32.

## Funções

### 1. `setup_printFlashMemoryStats()`

Esta função é responsável por inicializar o sistema de arquivos SPIFFS no ESP32.

- **Funcionalidade**: Inicializa o SPIFFS. Se necessário, o sistema de arquivos será formatado.
- **Parâmetros**: Nenhum.
- **Retorno**: Nenhum.

```c
void setup_printFlashMemoryStats() {
    if (!SPIFFS.begin(true)) {
        Serial.println("Erro ao montar SPIFFS");
        return;
    }
}
```

### 2. `printFlashMemoryStats()`

Esta função exibe as estatísticas da memória flash, incluindo o total de memória, memória usada e memória livre.

- **Funcionalidade**: Exibe as estatísticas da memória flash no monitor serial.
- **Parâmetros**: Nenhum.
- **Retorno**: Nenhum.

```c
void printFlashMemoryStats() {
    Serial.print("Memória Flash Total: ");
    Serial.print(SPIFFS.totalBytes() / 1024);
    Serial.println(" KB");

    Serial.print("Usados: ");
    Serial.print(SPIFFS.usedBytes());
    Serial.println(" KB");

    Serial.print("Livres: ");
    Serial.print((SPIFFS.totalBytes() - SPIFFS.usedBytes()) / 1024);
    Serial.println(" KB");
}
```

## Resumo

O arquivo `printFlashMemoryStats.ino` é focado em fornecer funções para inicializar o sistema de arquivos SPIFFS e exibir estatísticas relacionadas à memória flash do ESP32.

---

Deseja que eu continue detalhando os próximos arquivos ou prefere alguma ação específica neste momento?