/* 
  Esboço gerado pelo Arduino IoT Cloud Thing "Projeto IoT"
  https://create.arduino.cc/cloud/things/227fec5f-e11f-46d1-8708-e1474e6ff227 

  Descrição das Variáveis do Arduino IoT Cloud

  As seguintes variáveis são geradas automaticamente e atualizadas quando mudanças são feitas no Thing

  CloudLight led;

  Variáveis que são marcadas como LEITURA/ESCRITA no Cloud Thing também terão funções
  que são chamadas quando seus valores são alterados no Painel.
  Essas funções são geradas com o Thing e adicionadas ao final deste esboço.
*/

#include <ArduinoIoTCloud.h>
#include <Arduino_ConnectionHandler.h>

const char DEVICE_LOGIN_NAME[]  = "579e363e-f3a2-47a8-8a72-193ba714a7c6";

const char SSID[]        = "Lee";    // Nome da rede SSID
const char PASS[]        = "lee159753";    // Senha da rede (use para WPA, ou use como chave para WEP)
const char DEVICE_KEY[]  = "71VTOFQI779WBI9IFA1Y";    // Senha secreta do dispositivo

void onLedChange();

CloudLight led;

void initProperties(){
  ArduinoCloud.setBoardId(DEVICE_LOGIN_NAME);
  ArduinoCloud.setSecretDeviceKey(DEVICE_KEY);
  ArduinoCloud.addProperty(led, READWRITE, ON_CHANGE, onLedChange);
  Serial.println("Propriedades inicializadas.");
}

WiFiConnectionHandler ArduinoIoTPreferredConnection(SSID, PASS);

void setup() {
  Serial.begin(9600);
  delay(1500); 
  Serial.println("Iniciando setup...");

  initProperties();

  ArduinoCloud.begin(ArduinoIoTPreferredConnection);
  Serial.println("Conectando ao Arduino IoT Cloud...");

  setDebugMessageLevel(2);
  ArduinoCloud.printDebugInfo();
  Serial.println("Nível de debug configurado e informações de debug impressas.");
}

void loop() {
  ArduinoCloud.update();
  // Seu código aqui 
}

void onLedChange()  {
  Serial.println("Valor de 'led' alterado no IoT Cloud.");
}
