#include "ButtonHandler.h"

ButtonHandler::ButtonHandler(uint8_t pin) : pin(pin), previousState(HIGH), lastDebounceTime(0), buttonPressed(false) {
    pinMode(pin, INPUT_PULLUP);
}

bool ButtonHandler::isPressed() {
    bool currentState = digitalRead(pin);
    if (currentState != previousState) {
        lastDebounceTime = millis();
    }
    if (currentState == LOW && previousState == HIGH && (millis() - lastDebounceTime) > debounceDelay && !buttonPressed) {
        buttonPressed = true;
        previousState = currentState;
        return true;
    }
    previousState = currentState;
    return false;
}

bool ButtonHandler::isReleased() {
    bool currentState = digitalRead(pin);
    if (currentState != previousState) {
        lastDebounceTime = millis();
    }
    if (currentState == HIGH && previousState == LOW && buttonPressed) {
        buttonPressed = false;
        previousState = currentState;
        return true;
    }
    previousState = currentState;
    return false;
}
